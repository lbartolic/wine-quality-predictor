<?php

return [
    'WINES_PAGIN_NUM' => 9,
    'WINE_SCORE_LABELS' => [
    	1 => 'bad',
    	2 => 'average',
    	3 => 'great'
    ]
];