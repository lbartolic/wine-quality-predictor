<?php

return [
    'WINE_MAIN' => [
        'sugar' => 'required|numeric',
        'chlorides' => 'required|numeric',
        'density' => 'required|numeric',
        'ph' => 'required|numeric',
        'sulphates' => 'required|numeric',
        'alcohol' => 'required|numeric',
        'fixed_acidity' => 'required|numeric',
        'citric_acid' => 'required|numeric'
    ]
];