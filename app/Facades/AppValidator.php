<?php
namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class AppValidator extends Facade {
	protected static function getFacadeAccessor() { return 'AppValidatorClass'; }
}