<?php
namespace App\Facades\Classes;

use Eloquent;

class Azure extends Eloquent {
	public function getWineResults($inputsArray, $wineType) {
		if ($wineType == 0) {
			$serviceId = "f3f883ddd41e4c81b2e3fc49129624a4";
			$api_key = 'cYsY19ABV9RKYWQbcSHkQ0z3dUC++0isGV+30CPXuZcJb5CXK2yhkKTlMxnYSSwTBftY11ryqPZHhO7Aibm+Hw==';
		}
		else {
			$serviceId = "0a83cf98a2ee42539f0427e8fb445457";
			$api_key = 'ZjMSffglRbuFyAwVxMFw883dZGv7+TNsD98Bn8Hdi7aRNCcDMsxBfDmrnax3Ae0tHeVSS0hAUwOrb9CRVAtaqQ==';
		}

		$url = 'https://ussouthcentral.services.azureml.net/workspaces/6d270a4af6394b2c9b02d9d433906193/services/' . $serviceId . '/execute?api-version=2.0&details=true';

		$valuesArray = array();
		foreach($inputsArray as $inputArray) {
			$valuesArray[] = array(
				$inputArray['fixed_acidity'],
				"0",
				$inputArray['citric_acid'],
				$inputArray['sugar'],
				$inputArray['chlorides'],
				"0",
				"0",
				$inputArray['density'],
				$inputArray['ph'],
				$inputArray['sulphates'],
				$inputArray['alcohol'],
				"0"
			);
		}

    	$data = array(
			'Inputs' => array('input1' => array(
				'ColumnNames' => array(
					"fixed acidity",
			        "volatile acidity",
			        "citric acid",
			        "residual sugar",
			        "chlorides",
			        "free sulfur dioxide",
			        "total sulfur dioxide",
			        "density",
			        "pH",
			        "sulphates",
			        "alcohol",
			        "quality"
        		), 
				'Values' => $valuesArray
        	))
		);
		$body = json_encode($data);
		$headers = array('Content-Type: application/json', 'Authorization:Bearer ' . $api_key, 'Content-Length: ' . strlen($body));

		$curl = curl_init($url); 
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$response = json_decode(curl_exec($curl));
		$response->wine_type = $wineType;
		return $response;
	}
}