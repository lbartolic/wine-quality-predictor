<?php
namespace App\Facades\Classes;

use Eloquent;

class AppValidator extends Eloquent {
	protected $errors;
	protected $valid;

	public function __construct() {
		$this->valid = true;
	}

	public function makeForFileType($file, $extensions) {
        $extension = $file->getClientOriginalExtension();
        if (!in_array($extension, $extensions)) {
        	$this->valid = false;
        	$this->errors = ['file' => 'File type must be something of ' . implode(', ', $extensions) . '.'];
        }
        return $this;
    }

    /*
     * True if fails, otherwise false.
     */
    public function fails() {
    	return !($this->valid);
    }

    public function getErrors() {
    	return $this->errors;
    }
}