<?php
namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class Azure extends Facade {
	protected static function getFacadeAccessor() { return 'AzureClass'; }
}