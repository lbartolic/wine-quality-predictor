<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WineUpload extends Model
{
    protected $table = "wine_uploads";
    protected $fillable = [
    	'file'
    ];

	public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function wines() {
        return $this->hasMany('App\Models\Wine', 'upload_id');
    }

    public function getCountByScore($score) {
    	return $this->wines->filter(function($wine) use($score) {
            return $wine->scored_result == $score;
        })->count();
    }
}
