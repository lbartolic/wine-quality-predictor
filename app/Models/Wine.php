<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wine extends Model
{
	use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
    	'name',
    	'sugar',
    	'chlorides',
    	'density',
    	'ph',
    	'sulphates',
    	'alcohol',
    	'fixed_acidity',
    	'citric_acid',
    	'probabilities_1',
    	'probabilities_2',
    	'probabilities_3',
    	'scored_result',
        'wine_type'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function upload() {
    	return $this->belongsTo('App\Models\WineUpload', 'upload_id');
    }

    public function setShowUrlAttribute($url) {
        $this->attributes['show_url'] = $url;
    }

    public function getTypeNameAttribute() {
        return ($this->wine_type == 0) ? 'white' : 'red';
    }

    public function setHtmlClassScored($score) {
    	return $this->scored_result == $score ? '_wc-score-max' : '';
    }

    public function scopeWhiteWines($q) {
        return $q->where('wine_type', 0);
    }

    public function scopeRedWines($q) {
        return $q->where('wine_type', 1);
    }

    public function scopeScoredResult($q, $scoredResult) {
        return $q->where('scored_result', $scoredResult);
    }
}
