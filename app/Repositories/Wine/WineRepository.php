<?php 
namespace App\Repositories\Wine;

use Illuminate\Http\Request;
use Config;
use Schema;

use App\Models\User;
use App\Models\Wine;
use App\Repositories\Wine\WineCache;

use Auth;
use Excel;
use Validator;

class WineRepository {
	protected $wineCache;

    public function __construct(WineCache $wineCache) {
        $this->wineCache = $wineCache;
    }

    public function getWinesByUser(User $user) {
    	return Wine::where('user_id', $user->id)->where('active', 1);
    }

    public function limitWinesResult($wines, $limit) {
    	return $wines->take($limit);
    }

	public function getPaginatedWines(User $user, Request $request) {
		$wines = $this->getWinesByUser($user);
		/*
		 * Ordering logic depending on request input order_by parameter.
		 */
		if ($request->has('order_by')) {
			switch($request->input('order_by')) {
				case 'oldest':
					$wines = $wines->orderBy('created_at', 'asc');
					break;
				case 'latest':
					$wines = $wines->orderBy('created_at', 'desc');
					break;
				case 'A-Z':
					$wines = $wines->orderBy('name', 'asc');
					break;
				case 'Z-A':
					$wines = $wines->orderBy('name', 'desc');
					break;
			}
		}
		else $wines = $wines->orderBy('created_at', 'desc');

		$searchTerm = null;
		if ($request->has('search')) {
			$searchTerm = $request->input('search');
			$wines = $wines->where('name', 'LIKE', '%'. $searchTerm .'%');
		}

		$scoredResult = null;
		if ($request->has('scored_result')) {
			$scoredResult = $request->input('scored_result');
			$wines = $wines->where('scored_result', $scoredResult);
		}

		$wineType = null;
		if ($request->has('wine_type')) {
			$wineType = $request->input('wine_type');
			($wineType == 'white') ? $wines = $wines->where('wine_type', 0) : $wines = $wines->where('wine_type', 1);
		}

		$pagination_num = Config::get('constants.WINES_PAGIN_NUM');
		$winesPaginated = $wines->paginate($pagination_num);
    	$paginationTo = $winesPaginated->currentPage() * $pagination_num;
    	$paginationFrom = $paginationTo - $pagination_num + 1;
    	($paginationTo > $winesPaginated->total()) ? $paginationTo = $winesPaginated->total() : $paginationTo;
		
		return [
			'wines' => $winesPaginated,
			'filters' => array_except($request->all(), ['page']),
			'from' => $paginationFrom,
			'to' => $paginationTo,
			'searchTerm' => $searchTerm,
			'scoredResult' => $scoredResult,
			'wineType' => $wineType
		];
	}

	public function getScoredResultCount(Request $request) {
		return $this->wineCache->getScoredResultCount($request);
	}

	public function getWinesByTypeCount(Request $request) {
		return $this->wineCache->getWinesByTypeCount($request);
	}

	public function forgetScoredResultCount(User $user) {
		$this->wineCache->forgetScoredResultCount($user);
	}

	public function forgetWinesByTypeCount(User $user) {
		$this->wineCache->forgetWinesByTypeCount($user);
	}
}