<?php 
namespace App\Repositories\Wine;

use Illuminate\Http\Request;

use Cache;
use App\Models\User;

class WineCache {
	public function getScoredResultCount(Request $request) {
		$cacheKey = 'result_counts_' . $request->user()->id;

		return Cache::remember($cacheKey, 10, function() use($request) {
			$groups = $request->user()->wines->groupBy(['scored_result']);
			$resultGroupCounts = [];
			foreach($groups as $key => $value) {
				$resultGroupCounts[$key] = count($value);
			}
		    return $resultGroupCounts;
		});
	}
	public function forgetScoredResultCount(User $user) {
		$cacheKey = 'result_counts_' . $user->id;
		Cache::forget($cacheKey);
	}

	public function getWinesByTypeCount(Request $request) {
		$cacheKey = 'wines_by_type_count_' . $request->user()->id;
		
		return Cache::remember($cacheKey, 10, function() use($request) {
			$whiteWines = $request->user()->wines()->whiteWines()->count();
			$redWines = $request->user()->wines()->redWines()->count();
			return [$whiteWines, $redWines];
		});
	}
	public function forgetWinesByTypeCount(User $user) {
		$cacheKey = 'wines_by_type_count_' . $user->id;
		Cache::forget($cacheKey);
	}
}