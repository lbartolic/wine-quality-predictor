<?php 
namespace App\Repositories\WineUpload;

use Illuminate\Http\Request;
use Config;

use App\Models\User;
use App\Models\Wine;
use App\Models\WineUpload;
use App\Repositories\Wine\WineCache;

use Auth;
use Excel;
use Validator;
use Carbon\Carbon;
use App\Facades\Azure;

class WineUploadRepository {
    protected $wineCache;

    public function __construct(WineCache $wineCache) {
        $this->wineCache = $wineCache;
    }

	public function wineDatasetLoad($file) {
	   	$wineData = Excel::load($file, function($reader) {})->toArray();
	   	return $wineData;
	}

	public function storeAndAnalyze(User $user, $file, $wines, $wineType) {
        $storedFile = $this->storeDatasetFile($user, $file);
        $azureResult = Azure::getWineResults($wines, $wineType);

        foreach($wines as $index => $wine) {
            $resultValues = $azureResult->Results->output1->value->Values[$index];
            $wineObj = new Wine();
            $wineObj->name = "Unnamed wine";
            $wineObj->fill($wine);
            $wineObj->active = 0;
            $wineObj->probabilities_1 = $resultValues[9];
            $wineObj->probabilities_2 = $resultValues[10];
            $wineObj->probabilities_3 = $resultValues[11];
            $wineObj->scored_result = $resultValues[12];
            $wineObj->wine_type = $wineType;
            $wineObj->user()->associate($user);
            $storedFile->wines()->save($wineObj);
        }
        return $storedFile->id;
    }

    public function storeDatasetFile(User $user, $file) {
        $fileFullName = $file->getClientOriginalName();
        $fileName = pathinfo($fileFullName, PATHINFO_FILENAME);
        $fileExtension = pathinfo($fileFullName, PATHINFO_EXTENSION);
        $fileNewName = $fileName . '_' . Carbon::now()->timestamp . '.' . $fileExtension;
        $file->move(storage_path('app/public/uploads/wine_uploads'), $fileNewName);
        $storedFile = $user->wineUploads()->create(['file' => $fileNewName]);
        return $storedFile;
    }

    public function activateUploadedWines($id) {
    	WineUpload::findOrFail($id)->wines()->update(['active' => 1]);
        $this->wineCache->forgetScoredResultCount(Auth::user());
        $this->wineCache->forgetWinesByTypeCount(Auth::user());
    }

    public function destroyPendingDataset($id) {
    	$wineUpload = WineUpload::findOrFail($id);
        $wineUpload->wines()->forceDelete();
        $wineUpload->forceDelete();
    }
}