<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfPendingUploads
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $winesPending = $request->user()
            ->wines()
            ->where('active', 0);
        if ($winesPending->count() > 0) {
            $wineUpload = $winesPending->first()->upload;
            return redirect()->route('showWineUpload', [$wineUpload->id]);
        }

        return $next($request);
    }
}
