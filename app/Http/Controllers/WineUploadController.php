<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;

use App\Http\Requests;
use Carbon\Carbon;
use Session;
use Validator;
use App\Facades\AppValidator;

use App\Models\Wine;
use App\Models\WineUpload;
use App\Repositories\WineUpload\WineUploadRepository;
use App\Http\Requests\PostWineUploadRequest;
use App\Http\Requests\WineUpload\ActivateUploadedRequest;
use App\Http\Requests\WineUpload\DestroyUploadedRequest;

use App\Http\Middleware\RedirectIfPendingUploads;

class WineUploadController extends Controller
{
    protected $wineUploadRepo;

    public function __construct(WineUploadRepository $wineUploadRepo) {
        $this->wineUploadRepo = $wineUploadRepo;

        $this->middleware('RedirectIfPendingUploads', [
            'only' => ['index', 'postWineUpload']
        ]);
    }

    public function index(Request $request)
    {
        $wineUploads = $request->user()->wineUploads()->with('wines')->latest()->take(10)->get();
        return view('pages.wine_uploads.index')->with('wineUploads', $wineUploads);
    }

    public function show($id) {
        $wineUpload = WineUpload::findOrFail($id);
        $wines = $wineUpload->wines()->get();

        return view('pages.wine_uploads.show')
            ->with('wineUpload', $wineUpload)
            ->with('wines', $wines);
    }

    public function postWineUpload(Request $request) {
        $file = $request->file('dataset_file');
        $user = $request->user();
        $wineType = $request->input('wine_type');

        $fileValidator = AppValidator::makeForFileType($file, ['xls', 'csv']);
        if ($fileValidator->fails()) {
            return redirect()->back()->withErrors($fileValidator->getErrors());
        }

        $loadedWines = $this->wineUploadRepo->wineDatasetLoad($file);
        foreach($loadedWines as $wine) {
            $validator = Validator::make($wine, Config::get('validation_rules.WINE_MAIN'));
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }
        }

        $storedFile = $this->wineUploadRepo->storeAndAnalyze($user, $file, $loadedWines, $wineType);
        return redirect()->route('showWineUpload', $storedFile);
    }

    public function activateUploaded(ActivateUploadedRequest $request, $id) {
        $this->wineUploadRepo->activateUploadedWines($id);

        Session::flash('success', 'Wine dataset has been saved.');
        return redirect()->back();
    }

    public function destroyUploaded(ActivateUploadedRequest $request, $id) {
        $this->wineUploadRepo->destroyPendingDataset($id);

        Session::flash('success', 'Wine dataset has been removed. You may upload a new one.');
        return redirect()->route('wineUploadsHome');
    }
}
