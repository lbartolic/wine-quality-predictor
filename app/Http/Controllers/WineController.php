<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PostAzureWinePredictRequest;
use App\Http\Requests\Wine\WineShowRequest;
use App\Http\Requests\Wine\WineDestroyRequest;
use Auth;
use Session;

use App\Models\Wine;
use App\Repositories\Wine\WineRepository;
use App\Facades\Azure;

class WineController extends Controller
{
	protected $wineRepo;

    public function __construct(WineRepository $wineRepo) {
        $this->winesRepo = $wineRepo;
    }

    public function index(Request $request) {
    	$winesData = $this->winesRepo->getPaginatedWines(Auth::user(), $request);
        $wineScoredRes = $this->winesRepo->getScoredResultCount($request);
        $winesByType = $this->winesRepo->getWinesByTypeCount($request);
    	
    	return view('pages.wines.index', [
			'wines' => $winesData['wines'],
            'wineScoredRes' => $wineScoredRes,
            'winesByType' => $winesByType,
			'filters' => $winesData['filters'],
			'winesFrom' => $winesData['from'],
			'winesTo' => $winesData['to'],
			'searchTerm' => $winesData['searchTerm'],
			'scoredResult' => $winesData['scoredResult'],
            'wineType' => $winesData['wineType']
    	]);
    }

    public function store(Request $request) {
    	$request->user()->wines()->create($request->all());
        $this->winesRepo->forgetScoredResultCount($request->user());
        $this->winesRepo->forgetWinesByTypeCount($request->user());

    	Session::flash('success', 'Wine has been saved.');
    	return redirect()->back();
    }

    public function show(WineShowRequest $request, $id) {
    	if ($request->ajax()) {
	    	$wine = Wine::findOrFail($id);
			return response()->json($wine);
	    }
	    return redirect()->back();
    }

    public function destroy(WineDestroyRequest $request, $id) {
    	$wine = Wine::findOrFail($id);
    	$wine->delete();
        $this->winesRepo->forgetScoredResultCount($request->user());
        $this->winesRepo->forgetWinesByTypeCount($request->user());

    	Session::flash('success', 'Wine has been removed.');
    	return redirect()->back();
    }

    public function postAzureWinePredict(PostAzureWinePredictRequest $request) {
    	$azureInputs[] = $request->all();
        $wineType = $request->input('wine_type');
		$azureResult = Azure::getWineResults($azureInputs, $wineType);
		return response()->json($azureResult);
    }
}
