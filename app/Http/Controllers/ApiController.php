<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\User;
use App\Models\Wine;
use App\Repositories\Wine\WineRepository;
use App\Repositories\WineUpload\WineUploadRepository;

class ApiController extends Controller
{
	protected $wineRepo;
	protected $wineUploadRepo;

	public function __construct(WineRepository $wineRepo, WineUploadRepository $wineUploadRepo) {
		$this->wineRepo = $wineRepo;
		$this->wineUploadRepo = $wineUploadRepo;
	}

	public function getWineFilters($request, $wines) {
		($request->has('scored_result')) ? $wines = $wines->scoredResult($request->input('scored_result')) : $wines;
		($request->has('sort')) ? $wines = $wines->orderBy('name', $request->input('sort')) : $wines;
    	($request->has('limit')) ? $wines = $this->wineRepo->limitWinesResult($wines, $request->input('limit')) : $wines;
    	return $wines;
	}

    public function getWines(User $user, Request $request) {
    	$wines = $this->wineRepo->getWinesByUser($user);
    	return $this->getWineFilters($request, $wines)->get()->toJson();
    }

    public function getRedWines(User $user, Request $request) {
    	$wines = $this->wineRepo->getWinesByUser($user)->redWines();
    	return $this->getWineFilters($request, $wines)->get()->toJson();
    }

    public function getWhiteWines(User $user, Request $request) {
    	$wines = $this->wineRepo->getWinesByUser($user)->whiteWines();
    	return $this->getWineFilters($request, $wines)->get()->toJson();
    }
}
