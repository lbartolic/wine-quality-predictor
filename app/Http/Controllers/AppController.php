<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AppController extends Controller
{
    public function index() {
    	return redirect()->route('winesHome');
    }

    public function test() {
    	return sha1('123456') . sha1(time());
    }

    public function myApiIndex() {
    	return view('pages.api.index');
    }
}
