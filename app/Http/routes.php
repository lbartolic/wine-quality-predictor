<?php

Route::group(['middleware' => 'auth'], function() {
	Route::get('/', [
		'as' => 'home', 
		'uses' => 'AppController@index'
	]);
	Route::get('test', [
		'as' => 'test',
		'uses' => 'AppController@test'
	]);
	Route::get('my-api', [
		'as' => 'myApi',
		'uses' => 'AppController@myApiIndex'
	]);

	Route::post('wines/postAzureWinePredict', [
		'as' => 'postAzureWinePredict', 
		'uses' => 'WineController@postAzureWinePredict'
	]);
	Route::resource('wines', 'WineController', [
		'names' => ['index' => 'winesHome']
	]);

	Route::post('wine-uploads/postWineUpload', [
		'as' => 'postWineUpload',
		'uses' => 'WineUploadController@postWineUpload'
	]);
	Route::post('wine-uploads/postActivateUploaded/{id}', [
		'as' => 'postActivateUploaded',
		'uses' => 'WineUploadController@activateUploaded'
	]);
	Route::get('wine-uploads/analysis/{id}', [
		'as' => 'showWineUpload',
		'uses' => 'WineUploadController@show'
	]);
	Route::delete('wine-uploads/analysis/{id}', [
		'as' => 'destroyPendingDataset',
		'uses' => 'WineUploadController@destroyUploaded'
	]);
	Route::resource('wine-uploads', 'WineUploadController', [
		'names' => ['index' => 'wineUploadsHome']
	]);
});

Route::group(['prefix' => 'api/{user_api_key}'], function() {
    Route::get('wines', ['uses' => 'ApiController@getWines']);
	Route::get('wines/white', ['uses' => 'ApiController@getWhiteWines']);
	Route::get('wines/red', ['uses' => 'ApiController@getRedWines']);
});

// Authentication routes
Route::get('login', [
	'as' => 'getLogin',
	'uses' => 'Auth\AuthController@getLogin'
]);
Route::post('login', [
	'as' => 'postLogin',
	'uses' => 'Auth\AuthController@postLogin'
]);
Route::get('logout', [
	'as' => 'logout', 
	'uses' => 'Auth\AuthController@logout'
]);

// Registration routes
Route::get('register', [
	'as' => 'getRegister',
	'uses' => 'Auth\AuthController@getRegister'
]);
Route::post('register', [
	'as' => 'postRegister',
	'uses' => 'Auth\AuthController@postRegister'
]);