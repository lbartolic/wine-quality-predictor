<?php

namespace App\Http\Requests\WineUpload;

use App\Http\Requests\Request;
use App\Models\WineUpload;

class ActivateUploadedRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Request::user();
        $uploadedId = $this->route('id');
        return WineUpload::where('id', $uploadedId)->where('user_id', $user->id)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
