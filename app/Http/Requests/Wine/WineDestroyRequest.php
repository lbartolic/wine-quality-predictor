<?php

namespace App\Http\Requests\Wine;

use App\Http\Requests\Request;
use App\Models\Wine;

class WineDestroyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Request::user();
        $wineId = $this->route('wines');
        return Wine::where('id', $wineId)->where('user_id', $user->id)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
