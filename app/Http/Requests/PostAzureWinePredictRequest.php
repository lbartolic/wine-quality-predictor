<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Config;

class PostAzureWinePredictRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Config::get('validation_rules.WINE_MAIN');
        $rules = array_add($rules, 'wine_type', 'required|in:0,1');

        return $rules;
    }
}
