<?php

use Illuminate\Support\Arr;

function arraySet($array, $key, $value) {
	return Arr::set($array, $key, $value);
}

function isActiveGetParam($param, $value, $output = "active") {
    if (isset($_GET[$param]) && $_GET[$param] == $value) return $output;
}

function isSetGetParam($param) {
	return isset($_GET[$param]);
}