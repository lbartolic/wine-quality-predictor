HandlebarsIntl.registerWith(Handlebars);
Handlebars.registerHelper("handleNullValue", function(val) {
    if (val === undefined || val === null || val === "") {
    	return "–";
    }
    return val;
});
Handlebars.registerHelper("makePercentage", function(val) {
	var percentage = parseFloat(val)*100;
    return percentage;
});
Handlebars.registerHelper("scoredResult", function(val) {
	var scoredResultString;
	switch(parseInt(val)) {
		case 1:
			scoredResultString = 'bad';
			break;
		case 2:
			scoredResultString = 'average';
			break;
		case 3:
			scoredResultString = 'great';
			break;
	}
    return scoredResultString;
});
Handlebars.registerHelper("scoredResultColor", function(val) {
	var colorClass;
	switch(parseInt(val)) {
		case 1:
			colorClass = 'danger';
			break;
		case 2:
			colorClass = 'warning';
			break;
		case 3:
			colorClass = 'info';
			break;
	}
    return colorClass;
});
Handlebars.registerHelper("inc", function(value, options) {
    return parseInt(value) + 1;
});
Handlebars.registerHelper("wineShowUrl", function(id) {
    return APP_URL + '/wines/' + id;
});