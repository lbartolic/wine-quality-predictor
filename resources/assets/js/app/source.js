var mapAzureKeys = {
    alcohol: 							"alcohol",
    chlorides: 							"chlorides",
    citric_acid: 						"citric_acid",
    density: 							"density",
    fixed_acidity: 						"fixed_acidity",
    ph: 								"ph",
    residual_sugar: 					"sugar",
    scored_labels: 						"scored_result",
    scored_probabilities_for_class_1: 	"probabilities_1",
    scored_probabilities_for_class_2: 	"probabilities_2",
    scored_probabilities_for_class_3: 	"probabilities_3",
    sulphates: 							"sulphates"
};

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')
    }
});

function setupDeleteConfEvent() {
	$(document).ready(function() {
	    $('._delete-conf').off().on('click', function(){
	        var $form = $(this).closest("form");
	        swal({   
	            title: "Watch out",
	            text: "Are you sure you want to remove this?",
	            type: "warning",   
	            showCancelButton: true,   
	            confirmButtonColor: "rgb(55, 75, 103)",
	            confirmButtonHovercolor: "rgb(55, 75, 103)",
	            confirmButtonText: "Yes, remove it",
	            cancelButtonText: "Cancel",
	            closeOnConfirm: false
	        }, 
	        function() {   
	            $form.submit();
	        });
	    });
    });
}
setupDeleteConfEvent();

function setupLinkFormSubmit() {
	$(document).ready(function() {
	    $('._aFormSubmit').click(function(e) {
	    	e.preventDefault();
	    	$(this).closest('form').submit();
	    });
    });
}
setupLinkFormSubmit();


function showNotification(notifTitle, notifMsg, notifType, notifDelayInd) {
    var title = true,
        msg = '',
        type = 'success',
        delayIndicator = true;
            
    if (notifTitle !== undefined && notifTitle !== null) title = notifTitle;
    if (notifMsg !== undefined && notifMsg !== null) msg = notifMsg;
    if (notifType !== undefined && notifType !== null) type = notifType;
    if (notifDelayInd !== undefined && notifDelayInd !== null) delayIndicator = notifDelayInd;
    
    Lobibox.notify(type, {
        title: title,
        size: 'normal',
        icon: false,
        delayIndicator: delayIndicator,
        delay: 8000,
        rounded: false,
        position: 'center bottom',
        msg: msg,
        sound: false,
        showClass: 'fadeInUp',
        hideClass: 'fadeOutDown'
    });
}

function setNavbar(itemId) {
	$('.main-nav__item').removeClass('active');
	$('#' + itemId).addClass('active');
}

/**
 * Initialize JS DOM actions on each page load.
 */
$('document').ready(function() {
	$('[data-toggle="tooltip"]').tooltip();
	$('#azureWinePredict__submit').click(function (e) {
        e.stopPropagation();
        $(this).attr('disabled', 'disabled');
        var form = $(this).closest('form');
        postAzureWinePredict(form);
    });

    $('._showWine').click(function() {
    	var showUrl = $(this).attr('data-show');
    	showWine(showUrl);
    });

    $('#wineUpload__submit').click(function() {
    	showLoadingSpinner('#spinnerh', 'Uploading');
    });
    $('#datasetUpload__file-input').change(function() {
    	$('#wineUpload__submit').click();
	});
    $('._wineTableProperty').click(function(e) {
		e.preventDefault();
		e.stopPropagation();
		setWineUplTblProps(this);
	});
	$('._wineTableCheck').click(function(e) {
		e.preventDefault();
		setWineChecks(this);
	})
	$('#wineUpload__search').keyup(function() {
		searchUploadsTable($(this).val());
	});
	$('._uplTbl__row').click(function(e) {
		e.preventDefault();
		showAzureResults(this);
	});
});

function formatAzureResponse(data) {
	var dataFormated = {};
	for(var i = 0 ; i < data.Results.output1.value.ColumnNames.length ; i++) {
		var currentKey = data.Results.output1.value.ColumnNames[i].split(" ").join("_").toLowerCase().split('"').join('');
		var key = mapAzureKeys[currentKey] || currentKey;
		var value = parseFloat(data.Results.output1.value.Values[0][i]).toFixed(2);
		dataFormated[key] = value;
	}
	dataFormated['wine_type'] = data['wine_type'];
	return dataFormated;
}

function postAzureWinePredict(form) {
    var formAction = form.attr('action');
    var formData = form.serializeArray();
    formData.push({_method: 'POST'});
    console.log(formData);
    showLoadingSpinner('#new-prediction-well ._spinner-h');
    $.post(formAction, formData, function (r) {}, "json")
		.done(function(data) {
		    data = formatAzureResponse(data);
		    console.log(data);
   			showAzureResults(data);
		})
		.fail(function(data) {
		    console.log(data);
		    if (data.status == 422) {
		    	showNotification('Error', 'Some fields are not correct, please try again.', 'error');
		    }
		    if (data.status == 400) {
		        console.log(data.responseJSON);
		    }
		})
		.always(function() {
    		hideLoadingSpinner('#new-prediction-well ._spinner-h');
			$('#azureWinePredict__submit').removeAttr('disabled');
		});
}

function showAzureResults(data, onlyShow) {
	if (onlyShow === undefined || onlyShow === null) onlyShow = false;
	var jsonObj = data;
	var winePropertiesTemplate = document.getElementById('winePropertiesTable-hbt').innerHTML;
	var wineResultsTemplate = document.getElementById('wineResultsTable-hbt').innerHTML;
	document.getElementById('winePropertiesTable__holder').innerHTML = Handlebars.compile(winePropertiesTemplate)(jsonObj);
	document.getElementById('wineResultsTable__holder').innerHTML = Handlebars.compile(wineResultsTemplate)(jsonObj);
	if (onlyShow == true) {
		$('#wR-save__holder').hide();
	}
	else {
		var wineResultsInputsTemplate = document.getElementById('newWineResultsInputs-hbt').innerHTML;
		document.getElementById('wineResultsInputs__holder').innerHTML = Handlebars.compile(wineResultsInputsTemplate)(jsonObj);
		$('#wR-save__holder').show();
	}

	$('#chart__wPR-holder').find('#chart__winePredictionResult').remove();
	$('#chart__wPR-holder').html('<canvas id="chart__winePredictionResults"></canvas>');

	$('#azureResultsModal').on('shown.bs.modal open', function (event) {
		drawScoresChart(data, "#chart__winePredictionResults");
	});
	drawScoresChart(data, "#chart__winePredictionResults");

   	$('#azureResultsModal').modal('show');
}

function showWine(itemURL) {
	$.get(itemURL, function (r) {}, "json")
        .done(function(data) {
        	showAzureResults(data, true);
    	})
	    .fail(function (data) {
	        console.log(data);
	    });
}

/* ---------- WINE UPLOADS ---------- */

function makeWineUploadsTable(data) {
	if (data === null || data === undefined) data = JSON.parse(localStorage.getItem("wineUploadsJSON"));
	$('#winesUplTblCount').html(data.length);
	var winesData = {'wines': data};
	var wineUploadsTemplate = document.getElementById('uploadsAnalysisTable-hbt').innerHTML;
	document.getElementById('uplAnalysisTbl__holder').innerHTML = Handlebars.compile(wineUploadsTemplate)(winesData);
	setWineUplTblProps();
}

function setWineUplTblProps(thisElem) {
	if (thisElem !== undefined && thisElem !== null) {
		if ($(thisElem).hasClass('active')) {
			$(thisElem).removeClass('active').find('i').removeClass('fa-check-square-o').addClass('fa-square-o');
		}
		else {
			$(thisElem).addClass('active').find('i').removeClass('fa-square-o').addClass('fa-check-square-o');
		}
	}

	var showColumns = [];
	$('._wineTableProperty.active').each(function(){
		showColumns.push($(this).attr('data-column'));
	});
	$('#uplAnalysisTbl__holder td:nth-child(n+7), #uplAnalysisTbl__holder th:nth-child(n+7)').hide();
	for(var i = 0 ; i < showColumns.length ; i++) {
		$('#uplAnalysisTbl__holder td:nth-child(' + showColumns[i] + ')').show();
		$('#uplAnalysisTbl__holder th:nth-child(' + showColumns[i] + ')').show();
	}
}

function setWineChecks(thisElem) {
	var option = $(thisElem).attr('data-option');
	if (option == 'all') {
		$('._wineTableProperty').addClass('active').find('i').removeClass('fa-square-o').addClass('fa-check-square-o');
	}
	else {
		$('._wineTableProperty').removeClass('active').find('i').removeClass('fa-check-square-o').addClass('fa-square-o');
	}
	setWineUplTblProps();
}

function searchUploadsTable(searchTerm) {
	var searchTerm = $('#wineUpload__search').val();
	var data = JSON.parse(localStorage.getItem("wineUploadsJSON"));
	data = _.filter(data, function(wine) {
		return ((wine.name.toUpperCase()).indexOf(searchTerm.toUpperCase()) > -1);
	});
	makeWineUploadsTable(data);
}

/* ---------- WINE UPLOADS --------- */

function showLoadingSpinner(selector, text) {
	(text === undefined || text === null) ? text = 'Loading' : text;
	var loadingSpinner = $('body').find('._loadingSpinner__holder').clone();
	$(selector).append(loadingSpinner);
	$(selector).find('._loadingSpinner__holder').find('span').html()
	$(selector).find('._loadingSpinner__holder').show();
}

function hideLoadingSpinner(selector) {
	$(selector).find('._loadingSpinner__holder').remove();
}