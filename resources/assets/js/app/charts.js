function drawScoresChart(data, element, xScale, yScale) {
	(xScale == undefined || xScale == null) ? xScale = true : xScale;
	(yScale == undefined || yScale == null) ? yScale = true : xScale;
	var response = data;
   	var ctx = $(element);
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: {
	        labels: ["bad", "average", "great"],
	        datasets: [{
	        	label: '',
	            data: [data.probabilities_1*100, data.probabilities_2*100, data.probabilities_3*100],
	            backgroundColor: [
	                'rgba(217, 83, 79, 0.5)',
	                'rgba(240, 173, 78, 0.5)',
	                'rgba(91, 192, 222, 0.5)'
	            ],
	            hoverBackgroundColor: [
	            	'rgba(217, 83, 79, 0.7)',
	                'rgba(240, 173, 78, 0.7)',
	                'rgba(91, 192, 222, 0.7)'
	            ],
	            borderColor: [
	                'rgba(217, 83, 79, 1)',
	                'rgba(240, 173, 78, 1)',
	                'rgba(91, 192, 222, 1)'
	            ],
	            borderWidth: 1
	        }]
	    },

	    options: {
	    	legend: {
    			display: false
    		},
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero: true,
	                    max: 100
	                },
	                display: true
	            }],
	            xAxes: [{
	            	display: xScale
	            }]
	        },
	        tooltips: {
	        	enabled: true
	        }
	    }
	});
	console.log('asd');
}

function drawScoresPieChart(rawData, element, formatedData, animated) {
	(animated === undefined || animated === null) ? animated = true : animated;
	if (formatedData === undefined || formatedData === null) {
		var scoredResults = _.pluck(rawData, 'scored_result');
		var total = scoredResults.length;
		var scored1 = 0;
		var scored2 = 0;
		var scored3 = 0;
		scoredResults.forEach(function(score) {
			var scoreInt = parseInt(score);
			if (scoreInt == 1) scored1++;
			if (scoreInt == 2) scored2++;
			if (scoreInt == 3) scored3++;
		});
	}
	else {
		var scored1 = formatedData[1];
		var scored2 = formatedData[2];
		var scored3 = formatedData[3];
	}
	var ctx = $(element);
	var pieChart = new Chart(ctx, {
	    type: 'pie',
	    data: {
	        labels: ["bad", "avg.", "great"],
	        datasets: [{
	            data: [scored1, scored2, scored3],
	            backgroundColor: [
		            'rgba(217, 83, 79, 0.5)',
	                'rgba(240, 173, 78, 0.5)',
	                'rgba(91, 192, 222, 0.5)'
	            ],
	            hoverBackgroundColor: [
					'rgba(217, 83, 79, 0.7)',
	                'rgba(240, 173, 78, 0.7)',
	                'rgba(91, 192, 222, 0.7)'
	            ]
	        }]
	    },
	    options: {
	    	legend: {
    			display: true,
    			position: 'top',
    			onClick: function() { return false; },
    			labels: {
    				boxWidth: 6
    			}
    		},
	        tooltips: {
	        	enabled: true
	        },
	        animation: {
	        	animateRotate: animated
	        }
	    }
	});
}

function drawWineTypesChart(rawData, element, formatedData) {
	if (formatedData === undefined || formatedData === null) {
		
	}
	else {
		var whiteWines = formatedData[0];
		var redWines = formatedData[1];
	}
	var ctx = $(element);
	var pieChart = new Chart(ctx, {
	    type: 'pie',
	    data: {
	        labels: ["white", "red"],
	        datasets: [{
	            data: [whiteWines, redWines],
	            backgroundColor: [
		            'rgba(233, 224, 214, 0.7)',
		            'rgba(246, 194, 187, 0.9)'
	            ],
	            hoverBackgroundColor: [
		            'rgba(233, 224, 214, 0.8)',
		            'rgba(246, 194, 187, 1)'
	            ]
	        }]
	    },
	    options: {
	    	
	    	legend: {
    			display: true,
    			position: 'top',
    			onClick: function() { return false; },
    			labels: {
    				boxWidth: 6
    			}
    		},
	        tooltips: {
	        	enabled: true
	        },
	        animation: false
	    }
	});
}

function drawTotalScoresRadarChart(rawData, element, formatedData, animated) {
	(animated === undefined || animated === null) ? animated = true : animated;
	if (formatedData === undefined || formatedData === null) {}
	else {
		var whiteWinesData = formatedData[0];
		var redWinesData = formatedData[1];
	}
	console.log(formatedData);
	var ctx = $(element);
	var radarChart = new Chart(ctx, {
	    type: 'radar',
	    data: {
	        labels: ["bad", "avg.", "great"],
	        datasets: [
	        {
	            label: "White wines",
	            backgroundColor: "rgba(213, 201, 186, 0.2)",
	            borderColor: "rgba(213, 201, 186, 1)",
	            pointBackgroundColor: "rgba(213, 201, 186, 1)",
	            pointBorderColor: "#fff",
	            pointHoverBackgroundColor: "#fff",
	            pointHoverBorderColor: "rgba(213, 201, 186, 1)",
	            data: [whiteWinesData[1], whiteWinesData[2], whiteWinesData[3]]
	        },
	        {
	            label: "Red wines",
	            backgroundColor: "rgba(242, 152, 140, 0.2)",
	            borderColor: "rgba(242, 152, 140, 1)",
	            pointBackgroundColor: "rgba(242, 152, 140, 1)",
	            pointBorderColor: "#fff",
	            pointHoverBackgroundColor: "#fff",
	            pointHoverBorderColor: "rgba(242, 152, 140, 1)",
	            data: [redWinesData[1], redWinesData[2], redWinesData[3]]
	        }]
	    },
	    options: {
	    	legend: {
    			display: false,
    			position: 'top',
    			onClick: function() { return false; },
    			labels: {
    				boxWidth: 6
    			}
    		},
	        tooltips: {
	        	enabled: true
	        },
	        animation: {
	        	animateScale: animated
	        }
	    }
	});
}