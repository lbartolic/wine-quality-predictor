@extends('app')

@section('pageTitle', 'Login')
@section('bodyStyle', 'background: #3498db')

@section('main')

<div class="container-fluid">
    <div class="row _login-row">
        <div class="_login-holder">
            <form method="POST" action="/login">
                {!! csrf_field() !!}
                <h1>Wine Quality Prediction</h1>
                <input type="text" class="form-control" name="email" placeholder="your email" autofocus="" value="{{ old('email') }}">
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="your password">
                <button class="btn btn btn-primary btn-block" type="submit">Log In</button>
                <a href="/register"><button class="btn btn-sm btn-primary pull-right _marginTop10" type="button">Don't have an Account? Sign Up</button></a>
            </form>
        </div>
    </div>
</div>

@section('js')

@if (count($errors) > 0)
    <script>
        showNotification("Oops, error", "{{ $errors->all()[0] }}", "error");
    </script>
@endif

@endsection