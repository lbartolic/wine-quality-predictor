@extends('app')

@section('pageTitle', 'Login')
@section('bodyStyle', 'background: #3498db')

@section('main')

<div class="container-fluid">
    <div class="row _login-row">
        <div class="_login-holder">
            <form method="POST" action="/register">
                {!! csrf_field() !!}
                <h1>Wine Quality Prediction</h1>
                <input type="text" class="form-control" name="first_name" placeholder="your name" autofocus="" value="{{ old('first_name') }}">
                <input type="text" class="form-control" name="last_name" placeholder="your surename" autofocus="" value="{{ old('last_name') }}">
                <input type="text" class="form-control" name="email" placeholder="your email" autofocus="" value="{{ old('email') }}">
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="your password">
                <input type="password" id="inputPassword" name="password_confirmation" class="form-control" placeholder="your password again">
                <button class="btn btn btn-primary btn-block" type="submit">Sign Up</button>
            </form>
        </div>
    </div>
</div>

@section('js')

@if (count($errors) > 0)
    <script>
        showNotification("Oops, error", "{{ $errors->all()[0] }}", "error");
    </script>
@endif

@endsection