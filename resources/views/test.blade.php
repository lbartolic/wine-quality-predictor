<div class="container">
	<div class="row">
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			{!! Form::open(['method' => 'POST', 'action' => ['AppController@azurePredictionTest']]) !!}
				<legend>White Wine prediction testing</legend>
				<div class="form-group">
					<label for="ww_sugar">Sugar</label>
					<input type="text" class="form-control" id="ww_sugar" name="ww_sugar" placeholder="Sugar" value="1.6">
				</div>
				<div class="form-group">
					<label for="ww_chlorides">Chlorides</label>
					<input type="text" class="form-control" id="ww_chlorides" name="ww_chlorides" placeholder="Chlorides" value="0.049">
				</div>
				<div class="form-group">
					<label for="ww_density">Density</label>
					<input type="text" class="form-control" id="ww_density" name="ww_density" placeholder="Density" value="0.994">
				</div>
				<div class="form-group">
					<label for="ww_ph">pH-value</label>
					<input type="text" class="form-control" id="ww_ph" name="ww_ph" placeholder="pH-value" value="3.3">
				</div>
				<div class="form-group">
					<label for="ww_sulphates">Sulphates</label>
					<input type="text" class="form-control" id="ww_sulphates" name="ww_sulphates" placeholder="Sulphates" value="0.49">
				</div>
				<div class="form-group">
					<label for="ww_alcohol">Alcohol</label>
					<input type="text" class="form-control" id="ww_alcohol" name="ww_alcohol" placeholder="Alcohol" value="9.5">
				</div>

				<button type="submit" class="btn btn-primary">Submit</button>
			{!! Form::close() !!}
		</div>
		<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			<canvas id="myChart" width="400" height="400"></canvas>
		</div>
	</div>
</div>