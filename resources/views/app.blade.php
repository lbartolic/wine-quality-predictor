<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="csrf_token" content="{{ csrf_token() }}" />
      <link rel="icon" href="">
      <title>@yield('pageTitle') · Wine Quality Prediction</title>
      
      {!! Html::style('assets/css/all.css') !!}
      {!! Html::style('https://fonts.googleapis.com/css?family=Lato:400,300,700,900&subset=latin,latin-ext') !!}
      @yield('additionalCss')

      {!! Html::script('assets/js/all.js') !!}
      @yield('additionalJs')
   </head>

   <body class="@yield('bodyClass')" style="@yield('bodyStyle')">
       @yield('navbar')

       @yield('top')

       @yield('main')

       @yield('footer')
   </body>
</html>
<script>
  var APP_URL = {!! json_encode(url('/')) !!};
</script>

@yield('js')