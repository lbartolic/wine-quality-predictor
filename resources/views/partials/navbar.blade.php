<nav class="navbar navbar-default _navbar-main" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{ url('/') }}">Wine Quality Predictor</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li class="main-nav__item" id="nav__contacts"><a href="{{ route('home') }}">My Wines</a></li>
				<li class="main-nav__item" id="nav__map"><a href="{{ route('wineUploadsHome') }}">Wine Data Upload</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle text-capitalize" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>{{ Auth::user()->first_name }}</a>
					<ul class="dropdown-menu _dropdown-smaller _dropdown-darker">
						<li><a href="{{ route('myApi') }}"><i class="fa fa-cloud-download"></i>My API</a></li>
						<li role="separator" class="divider"></li>
						{!! Form::open(['method' => 'GET', 'url' => ['logout']]) !!}
        					<li><a href="#" class="_aFormSubmit"><i class="fa fa-sign-out"></i>Logout</a></li>
    					{!! Form::close() !!}
					</ul>
				</li>
				<div style="display: block;">
				
				</div>
			</ul>
		</div>
	</div>
</nav>