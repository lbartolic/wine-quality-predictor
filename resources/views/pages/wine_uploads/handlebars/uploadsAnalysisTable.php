<script id="uploadsAnalysisTable-hbt" type="text/x-handlebars-template">
	<table class="table table-bordered table-hover _table-content-mid">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Scored Result</th>
				<th><span class="label label-danger">bad</span></b></th>
				<th><span class="label label-warning">average</span></th>
				<th><span class="label label-info">great</span></b></th>
				<th>Sugar</th>
				<th>Chlorides</th>
				<th>Density</th>
				<th>pH value</th>
				<th>Sulphates</th>
				<th>Alcohol</th>
				<th>Fixed acidity</th>
				<th>Citric acid</th>
			</tr>
		</thead>
		<tbody>
			{{#each wines}}
				<tr class="_uplTbl__row _showWine" data-show="{{wineShowUrl id}}">
					<td>{{inc @index}}</td>
					<td>{{name}}</td>
					<td class="{{ scoredResultColor scored_result }} _uppercase"><b><small>{{ scoredResult scored_result }}</small></b></td>
					<td>{{makePercentage probabilities_1}}%</td>
					<td>{{makePercentage probabilities_2}}%</td>
					<td>{{makePercentage probabilities_3}}%</td>
					<td>{{sugar}}</td>
					<td>{{chlorides}}</td>
					<td>{{density}}</td>
					<td>{{ph}}</td>
					<td>{{sulphates}}</td>
					<td>{{alcohol}}</td>
					<td>{{fixed_acidity}}</td>
					<td>{{citric_acid}}</td>
				</tr>
			{{/each}}
		</tbody>
	</table>
</script>