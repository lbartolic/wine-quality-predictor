@extends('app')

@section('pageTitle', 'Wine Data Analysis')

@section('navbar')

@include('partials.navbar')

@endsection

@section('main')
@include('pages.wines.includes.modalAzureResults')
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
			<div class="well clearfix _content-card">
				<div class="_wine-content-topbar clearfix">
					<div class="row">
						<div class="_wc-topbar-left col-sm-6 col-xs-12">
							<div class="input-group">
								<input type="text" class="form-control" id="wineUpload__search" name="search" value="" placeholder="Search...">
								<span class="input-group-btn">
									<button type="submit" class="btn btn-default"><i class="fa fa-search _fa-reset _fa-full"></i></button>
								</span>
							</div>
						</div>
						<div class="_wc-topbar-right col-sm-6 col-xs-12">
							<div class="form-inline clearfix pull-right">
								<div class="form-group">
									<span id="pagin-info" class="_paginationInfo">Total: <b id="winesUplTblCount">{{ $wines->count() }}</b></span>
									<div class="btn-group _tooltip" data-toggle="tooltip" data-placement="top" role="group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-check-square-o _fa-full"></i>Properties</button>
										<ul id="wineUpload__props" class="dropdown-menu _dropdown-smaller pull-right _dropdown-wc-filter">
											<li class="_wineTableCheck" data-option="all">
												<a href="#">
													Check All
												</a>
											</li>
											<li class="_wineTableCheck" data-option="none">
												<a href="#">
													Uncheck All
												</a>
											</li>
											<li role="separator" class="divider"></li>
											<li class="active _wineTableProperty" data-column="7">
												<a href="#">
													<i class="fa fa-check-square-o"></i>Sugar
												</a>
											</li>
											<li class="_wineTableProperty" data-column="8">
												<a href="#">
													<i class="fa fa-square-o"></i>Chlorides
												</a>
											</li>
											<li class="active _wineTableProperty" data-column="9">
												<a href="#">
													<i class="fa fa-check-square-o"></i>Density
												</a>
											</li>
											<li class="_wineTableProperty" data-column="10">
												<a href="#">
													<i class="fa fa-square-o"></i>pH value
												</a>
											</li>
											<li class="_wineTableProperty" data-column="11">
												<a  href="#">
													<i class="fa fa-square-o"></i>Sulphates
												</a>
											</li>
											<li class="active _wineTableProperty" data-column="12">
												<a href="#">
													<i class="fa fa-check-square-o"></i>Alcohol
												</a>
											</li>
											<li class="_wineTableProperty" data-column="13">
												<a href="#">
													<i class="fa fa-square-o"></i>Fixed acidity
												</a>
											</li>
											<li class="_wineTableProperty" data-column="14">
												<a href="#">
													<i class="fa fa-square-o"></i>Citric acid
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@include('pages.wine_uploads.handlebars.uploadsAnalysisTable')
				<div id="uplAnalysisTbl__holder" class="table-responsive" style="max-height: 500px; overflow-y: scroll;">
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="row">
				<div class="col-sm-12">
					@if ($wines->first()->active == 0)
						<div class="alert _alert-sm _alert-main alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
							<i class="fa fa-warning"></i>Save or remove this dataset before uploading a new one.
						</div>
					@else 
						<div class="alert _alert-sm _alert-sec alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
							<i class="fa fa-check"></i>This dataset is saved.
						</div>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-6 col-xs-6">
					<div class="well clearfix _content-card">
						<h3 class="_content__title">Scored results</h3>
						<canvas id="chart__wineScores" height=250></canvas>
					</div>
				</div>
				<div class="col-md-12 col-sm-6 col-xs-6">
					<div class="well clearfix _content-card">
						<h3 class="_content__title">Dataset details</h3>
						<p>Type of wines: <b class="text-uppercase">{{ $wines->first()->type_name }}</b></p>
						<p>Uploaded on: <br>{{ $wineUpload->created_at->toFormattedDateString() }} <small>({{ $wineUpload->created_at->diffForHumans() }})</small></p>
						<ul class="list-group">
							<li class="list-group-item">
								<span class="badge _badge-main">{{ $wineUpload->getCountByScore(1) }}</span>
								<span class="label label-danger _label-full">bad</span>
							</li>
							<li class="list-group-item">
								<span class="badge _badge-main">{{ $wineUpload->getCountByScore(2) }}</span>
								<span class="label label-warning _label-full">average</span>
							</li>
							<li class="list-group-item">
								<span class="badge _badge-main">{{ $wineUpload->getCountByScore(3) }}</span>
								<span class="label label-info _label-full">great</span>
							</li>
						</ul>
					</div>
					@if ($wines->first()->active == 0)
						<div class="btn-group btn-group-justified _btn-group-block" role="group">
							<div class="btn-group" role="group">
								{!! Form::open(['method' => 'DELETE', 'route' => ['destroyPendingDataset', $wineUpload->id]]) !!}
									<button type="submit" class="btn btn-default">Remove</button>
								{!! Form::close() !!}
							</div>
							<div class="btn-group" role="group">
								{!! Form::open(['method' => 'POST', 'route' => ['postActivateUploaded', $wineUpload->id]]) !!}
									<button type="submit" class="btn btn-primary">Save</button>
								{!! Form::close() !!}
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
	
</div>
<div class="_fade"></div>
@include('partials.loadingSpinner')

@endsection

@section('js')

@if(Session::has('success'))
	<script>
		showNotification('Good job', "{{ Session::get('success') }}", 'success');
	</script>
@endif

<script>
	var wineData = {!!json_encode($wines)!!};
	localStorage.setItem("wineUploadsJSON", JSON.stringify(wineData));
	drawScoresPieChart(wineData, "#chart__wineScores");
	makeWineUploadsTable();
</script>

@endsection