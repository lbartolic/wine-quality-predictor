@extends('app')

@section('pageTitle', 'Wine Uploads')

@section('navbar')

@include('partials.navbar')

@endsection

@section('main')

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
			<div class="well clearfix _content-card">
				<h3 class="_content__title">Dataset file structure</h3>
				<span class="label label-default _label-med">Allowed file types</span>
				<p class="_marginTop6"><code>.csv</code>, <code>.xls</code></p>
				<span class="label label-default _label-med">Header (attributes)</span>
				<p class="_marginTop6"><samp>"name", "fixed acidity", "citric acid", "sugar", "chlorides", "density", "ph", "sulphates", "alcohol"</samp></p>
				<span class="label label-default _label-med">Body (types)</span>
				<p class="_marginTop6"><code>string</code>, <code>float</code>, <code>float</code>, <code>float</code>, <code>float</code>, <code>float</code>, <code>float</code>, <code>float</code>, <code>float</code></p>
			</div>
			<div class="well clearfix _content-card">
				<p>Select the wine type of your dataset and upload it:</p>
				{!! Form::open(['method' => 'POST', 'route' => ['postWineUpload'], 'files' => true]) !!}
					<div class="row _marginBtm15">
						<div class="col-xs-12 col-sm-4 _marginTop6">
							@include('pages.wines.includes.wineTypeRadioBtns')
						</div>
						<div class="col-xs-12 col-sm-4 _marginTop6">
							<label class="file-upload-main">
							    <input type="file" id="datasetUpload__file-input" name="dataset_file">
							    <span>Select a Dataset</span>
							</label>
						</div>
					</div>
					<button type="submit" style="display: none;" id="wineUpload__submit"></button>
				{!! Form::close() !!}
				<div id="spinnerh"></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<div class="well clearfix _content-card">
				<h3 class="_content__title">Recent uploads</h3>
				<div class="table-responsive">
					<table class="table table-striped" style="max-width: 100%;">
						<thead>
							<tr>
								<th>File</th>
								<th>Records</th>
								<th>Scores</th>
							</tr>
						</thead>
						<tbody>
							@foreach($wineUploads as $wineUpload)
								<tr>
									<td class="_text-ellipsis" style="max-width: 140px;" title="{{ $wineUpload->file }}"><i class="fa fa-file-text-o _fa-full"></i>
										<a class="_a-main" href="{{ route('showWineUpload', $wineUpload->id) }}">{{ $wineUpload->file }}</a>
									</td>
									<td>{{ $wineUpload->wines->count() }}</td>
									<td><span class="label label-danger _label-med">{{ $wineUpload->getCountByScore(1) }}</span> <span class="label label-warning _label-med">{{ $wineUpload->getCountByScore(2) }}</span> <span class="label label-info _label-med">{{ $wineUpload->getCountByScore(3) }}</span></td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="_fade"></div>
@include('partials.loadingSpinner')

@endsection

@section('js')

@if(Session::has('success'))
	<script>
		showNotification('Good job', "{{ Session::get('success') }}", 'success');
	</script>
@endif
@if (count($errors) > 0)
    <script>
        showNotification("Oops, error", "{{ $errors->all()[0] }}", "error");
    </script>
@endif


@endsection