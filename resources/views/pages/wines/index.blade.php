@extends('app')

@section('pageTitle', 'Homepage')

@section('navbar')

@include('partials.navbar')

@endsection

@section('main')

@include('pages.wines.includes.modalAzureResults')
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
			<div class="well clearfix _content-card">
				<div class="_wine-content-topbar clearfix">
					<div class="row">
						<div class="_wc-topbar-left col-sm-5 col-xs-12">
							{!! Form::open(['method' => 'GET', 'route' => ['winesHome']]) !!}
								<div class="input-group">
									<input type="text" class="form-control" name="search" value="{{ $searchTerm or '' }}" placeholder="Search...">
									<span class="input-group-btn">
										<button type="submit" class="btn btn-default" id="contacts-search__btn" type="button"><i class="fa fa-search _fa-reset _fa-full"></i></button>
									</span>
								</div>
							{!! Form::close() !!}
						</div>
						<div class="_wc-topbar-right col-sm-7 col-xs-12">
							<div class="form-inline clearfix pull-right">
								<div class="form-group">
									<span id="pagin-info" class="_paginationInfo"><b id="pagin-info__fromNum">{{ $winesFrom }}</b><b> – </b><b id="pagin-info__toNum">{{ $winesTo }}</b> of <b id="pagin-info__totalNum">{{ $wines->total() }}</b></span>
									
									{!! $wines->appends($filters)->render() !!}
									
									<div class="btn-group _tooltip" data-toggle="tooltip" data-placement="top" role="group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter _fa-reset _fa-full"></i></button>
										<ul class="dropdown-menu _dropdown-smaller pull-right _dropdown-wc-filter">
											<li class="{{ isActiveGetParam('order_by', 'oldest') }}">
												<a href="{{ route('winesHome', arraySet($filters, 'order_by', 'oldest')) }}">
													<i class="fa fa-sort-numeric-asc"></i>Oldest tests
												</a>
											</li>
											<li class="{{ (!isSetGetParam('order_by')) ? 'active' : isActiveGetParam('order_by', 'latest') }}">
												<a href="{{ route('winesHome', arraySet($filters, 'order_by', 'latest')) }}">
													<i class="fa fa-sort-numeric-desc"></i>Latest tests
												</a>
											</li>
											<li role="separator" class="divider"></li>
											<li class="{{ isActiveGetParam('order_by', 'A-Z') }}">
												<a href="{{ route('winesHome', arraySet($filters, 'order_by', 'A-Z')) }}">
													<i class="fa fa-sort-alpha-asc"></i>From A to Z
												</a>
											</li>
											<li class="{{ isActiveGetParam('order_by', 'Z-A') }}">
												<a href="{{ route('winesHome', arraySet($filters, 'order_by', 'Z-A')) }}">
													<i class="fa fa-sort-alpha-desc"></i>From Z to A
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="_wine-content clearfix" id="wine-content">
					<div class="_enabled-filters clearfix _marginBtm20">
						@if (isset($searchTerm))
							<div class="_label-filter">Search results for <b>{{ $searchTerm }}</b> <a href="{{ route('winesHome', array_except($filters, ['search'])) }}" class=""><i class="fa fa-close _fa-reset _fa-full"></i></a></div>
						@endif
						@if (isset($scoredResult))
							<div class="_label-filter">Filtered: <b>{{ $wineScoreLabels[$scoredResult] }}</b> scores <a href="{{ route('winesHome', array_except($filters, ['scored_result'])) }}" class=""><i class="fa fa-close _fa-reset _fa-full"></i></a></div>
						@endif
						@if (isset($wineType))
							<div class="_label-filter">Filtered: <b>{{ $wineType }}</b> wine <a href="{{ route('winesHome', array_except($filters, ['wine_type'])) }}" class=""><i class="fa fa-close _fa-reset _fa-full"></i></a></div>
						@endif
					</div>
					@foreach ($wines as $wine)
						<div class="_wc-item">
							<div class="_wc-item-holder">
								<div class="_wc-i-top">
									<h2 class="_text-ellipsis">{{ $wine->name }}</h2>
								</div>
								<div class="_wc-i-content">
									<div class="_wc-main-info clearfix">
										<div class="_wc-mi-block pull-left">
											<p><span style="display: block;">Created on</span> <b>{{ $wine->created_at->format('d/m/Y') }}</b></p>
										</div>
										<div class="_wc-mi-block pull-left">
											<p><span style="display: block;">Wine type</span>
												<b class="{{ ($wine->wine_type == 0) ? '_wine__w' : '_wine__r' }}">
													<a class="_a-clear _a__filterable" href="{{ route('winesHome', arraySet($filters, 'wine_type', $wine->type_name)) }}">
														{{ $wine->type_name }}
													</a>
												</b>
											</p>
										</div>
									</div>
									<div class="_wc-i-info-holder clearfix _info-basic">
										<div class="_wc-score-row clearfix">
											<div class="col-xs-4 _wc-score-box {{ $wine->setHtmlClassScored(1) }}">
												<div>
													<a class="_a-clear _a__filterable" href="{{ route('winesHome', arraySet($filters, 'scored_result', 1)) }}">
														<div class="_wc-score__value">
															{{ ($wine->probabilities_1)*100 }}<small>%</small>
														</div>
														<div class="_wc-score__info">
															{{ $wineScoreLabels[1] }}
														</div>
													</a>
												</div>
											</div>
											<div class="col-xs-4 _wc-score-box {{ $wine->setHtmlClassScored(2) }}">
												<div>
													<a class="_a-clear _a__filterable" href="{{ route('winesHome', arraySet($filters, 'scored_result', 2)) }}">
														<div class="_wc-score__value">
															{{ ($wine->probabilities_2)*100 }}<small>%</small>
														</div>
														<div class="_wc-score__info">
															{{ $wineScoreLabels[2] }}
														</div>
													</a>
												</div>
											</div>
											<div class="col-xs-4 _wc-score-box {{ $wine->setHtmlClassScored(3) }}">
												<div>
													<a class="_a-clear _a__filterable" href="{{ route('winesHome', arraySet($filters, 'scored_result', 3)) }}">
														<div class="_wc-score__value">
															{{ ($wine->probabilities_3)*100 }}<small>%</small>
														</div>
														<div class="_wc-score__info">
															{{ $wineScoreLabels[3] }}
														</div>
													</a>
												</div>
											</div>
										</div>
									</div>
									<div class="_wc-i-actions clearfix">
										<button class="btn btn-xs btn-primary _wc-expand-btn _showWine" data-show="{{ route('wines.show', [$wine->id]) }}">
											<i class="fa fa-expand _fa-reset _fa-full"></i>
										</button>
										<div class="btn-group pull-right _tooltip" data-toggle="tooltip" data-placement="top" title="Više">
											<button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fa fa-ellipsis-h _fa-reset _fa-full"></i>
											</button>
											<ul class="dropdown-menu _dropdown-smaller">
												<li><a href="#" class="_showWine" data-show="{{ route('wines.show', [$wine->id]) }}"><i class="fa fa-expand"></i>See full details</a></li>
												<li role="separator" class="divider"></li>
												{!! Form::open(['method' => 'DELETE', 'route' => ['wines.destroy', $wine->id]]) !!}
													<li><a href="#" class="_delete-conf"><i class="fa fa-remove"></i>Remove</a></li>
													<input type="hidden" name="id" value="">
													<input type="hidden" name="_action" value="delete_contact">
												</form>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<div class="row">
				<div class="col-sm-12">
					<div class="well clearfix _new-prediction-well _content-card" id="new-prediction-well">
						{!! Form::open(['method' => 'POST', 'route' => ['postAzureWinePredict'], 'id' => 'new-prediction-form']) !!}
							<h3 class="_content__title">New prediction test</h3>
							@include('pages.wines.includes.form')
							<div class="_spinner-h"></div>
							<button type="button" id="azureWinePredict__submit" class="btn btn-primary pull-right">Predict now</button>
							<button type="reset" class="btn btn-default pull-right _marginRight6">Cancel</button>
							<input type="hidden" name="_action" value="save_contact">
						{!! Form::close() !!}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="well clearfix _content-card">
						<h3 class="_content__title">Wine stats</h3>
						<div class="col-xs-offset-2 col-xs-8 col-sm-offset-2 col-sm-4 col-md-offset-2 col-md-8 col-lg-offset-0 col-lg-6">
							<canvas id="chart__whiteVsRed" height="400"></canvas>
						</div>
						<div class="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-4 col-md-offset-2 col-md-8 col-lg-offset-0 col-lg-6">
							<canvas id="chart__totalScores" height="400"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<div class="_fade"></div>
@include('partials.loadingSpinner')

@endsection

@section('js')

@if(Session::has('success'))
	<script>
		showNotification('Good job', "{{ Session::get('success') }}", 'success');
	</script>
@endif

<script>
	setupWinesIndexCharts();
	var totalScores = {!! json_encode($wineScoredRes) !!};
	var wineTypes = {!! json_encode($winesByType) !!};
	console.log(totalScores);
	drawScoresPieChart(null, "#chart__totalScores", totalScores, false);
	drawWineTypesChart(null, "#chart__whiteVsRed", wineTypes);
	function setupWinesIndexCharts() {

	}
</script>

@endsection