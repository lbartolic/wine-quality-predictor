<div class="btn-group btn-group-justified" data-toggle="buttons">
	<label class="btn btn-primary active">
		<input type="radio" name="wine_type" value="0" autocomplete="off" checked> White wine
	</label>
	<label class="btn btn-primary">
		<input type="radio" name="wine_type" value="1" autocomplete="off"> Red wine
	</label>
</div>