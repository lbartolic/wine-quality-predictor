<div class="row _marginBtm15">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		@include('pages.wines.includes.wineTypeRadioBtns')
	</div>
</div>
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
		<div class="form-group">
			<label for="form_ime">Sugar</label>
			<input type="number" class="form-control input-sm" name="sugar" id="form_sugar" placeholder="sugar" data-validation="required" data-validation-error-msg="Ime je potrebno" value="{{ old('sugar') }}">
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
		<div class="form-group">
			<label for="form_prezime">Chlorides</label>
			<input type="number" class="form-control input-sm" name="chlorides" id="form_chlorides" placeholder="chlorides" data-validation="required" data-validation-error-msg="Prezime je potrebno" value="{{ old('chlorides') }}">
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
		<div class="form-group">
			<label for="form_ime">Density</label>
			<input type="number" class="form-control input-sm" name="density" id="form_density" placeholder="density" data-validation="required" data-validation-error-msg="Ime je potrebno" value="{{ old('density') }}">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
		<div class="form-group">
			<label for="form_prezime">pH value</label>
			<input type="number" class="form-control input-sm" name="ph" id="form_phValue" placeholder="pH value" data-validation="required" data-validation-error-msg="Prezime je potrebno" value="{{ old('phValue') }}">
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
		<div class="form-group">
			<label for="form_ime">Sulphates</label>
			<input type="number" class="form-control input-sm" name="sulphates" id="form_sulphates" placeholder="sulphates" data-validation="required" data-validation-error-msg="Ime je potrebno" value="{{ old('sulphates') }}">
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
		<div class="form-group">
			<label for="form_prezime">Alcohol</label>
			<input type="number" class="form-control input-sm" name="alcohol" id="form_alcohol" placeholder="alcohol" data-validation="required" data-validation-error-msg="Prezime je potrebno" value="{{ old('alcohol') }}">
		</div>
	</div>
</div>
<div class="row">
</div>
<div class="row">
	<div class="col-xs-4 col-sm-4 col-md-6 col-lg-4">
		<div class="form-group">
			<label for="form_ime">Fixed acidity</label>
			<input type="number" class="form-control input-sm" name="fixed_acidity" id="form_fixed_acidity" placeholder="fixed acidity" data-validation="required" data-validation-error-msg="Ime je potrebno" value="{{ old('fixed_acidity') }}">
		</div>
	</div>
	<div class="col-xs-4 col-sm-4 col-md-6 col-lg-4">
		<div class="form-group">
			<label for="form_prezime">Citric acid</label>
			<input type="number" class="form-control input-sm" name="citric_acid" id="form_citric_acid" placeholder="citric acid" data-validation="required" data-validation-error-msg="Prezime je potrebno" value="{{ old('citric_acid') }}">
		</div>
	</div>
</div>