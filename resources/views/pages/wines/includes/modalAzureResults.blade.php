<div class="modal" id="azureResultsModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog pulse animated">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Wine score prediction results</h4>
			</div>
			<div class="modal-body">
				<div class="clearfix">
					<h4 class="_marginTop0">Wine properties (inputs)</h4>
				</div>
				@include('pages.wines.handlebars.winePropertiesTable')
				<div id="winePropertiesTable__holder" class="table-responsive"></div>

				<h4>Prediction results (outputs)</h4>
				<div id="chart__wPR-holder">
					<canvas id="chart__winePredictionResults" height="280"></canvas>
				</div>
				@include('pages.wines.handlebars.wineResultsTable')
				<div id="wineResultsTable__holder" class="_marginTop15 table-responsive"></div>

				<div id="wR-save__holder">
					<button class="btn btn-primary btn-block" data-toggle="collapse" href="#wR-save__collapse">
						<i class="fa fa-save _fa-full"></i>Save these results ...
					</button>
					<div id="wR-save__collapse" class="collapse">
						<div class="panel panel-default">
							<div class="panel-body">
							{!! Form::open(['method' => 'POST', 'action' => ['WineController@store']]) !!}
								<div class="form-group">
									<label for="name">Title</label>
									<input type="text" class="form-control input-lg" name="name" placeholder="wine prediction test title" data-validation="required" data-validation-error-msg="Ime je potrebno">
								</div>
								@include('pages.wines.handlebars.newWineResultsInputs')
								<div id="wineResultsInputs__holder"></div>
								<button type="submit" class="btn btn-primary pull-right">Save</button>
							{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>