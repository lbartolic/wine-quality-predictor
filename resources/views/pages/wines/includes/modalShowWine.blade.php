<div class="modal" id="showWineModal" tabindex="-1" role="dialog">
	<div class="modal-dialog pulse animated">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="showWine__title"></h4>
			</div>
			<div class="modal-body">
				<div class="clearfix">
					<h4 class="_marginTop0">Wine properties (inputs)</h4>
				</div>
				@include('pages.wines.handlebars.winePropertiesTable')
				<div id="showWine__propertiesTable" class="table-responsive"></div>

				<h4>Prediction results (outputs)</h4>
				<div id="chart__wPR-holder">
					<canvas id="chart__wineResults"></canvas>
				</div>
				@include('pages.wines.handlebars.wineResultsTable')
				<div id="showWine__resultsTable" class="_marginTop15 table-responsive"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>