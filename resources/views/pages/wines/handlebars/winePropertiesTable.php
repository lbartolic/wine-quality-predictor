<script id="winePropertiesTable-hbt" type="text/x-handlebars-template">
	<table class="table table-bordered">
		<tr>
			<th>Sugar</th>
			<th>Chlorides</th>
			<th>Density</th>
			<th>pH value</th>
			<th>Sulphates</th>
			<th>Alcohol</th>
			<th>Fixed acidity</th>
			<th>Citric acid</th>
		</tr>
		<tr>
			<td>{{ sugar }}</td>
			<td>{{ chlorides }}</td>
			<td>{{ density }}</td>
			<td>{{ ph }}</td>
			<td>{{ sulphates }}</td>
			<td>{{ alcohol }}</td>
			<td>{{ fixed_acidity }}</td>
			<td>{{ citric_acid }}</td>
		</tr>
	</table>
</script>