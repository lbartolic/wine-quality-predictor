<script id="wineResultsTable-hbt" type="text/x-handlebars-template">
	<table class="table table-bordered">
		<tr>
			<th>Scored Result</th>
			<th>Chances for <span class="label label-danger">bad</span></b></th>
			<th>Chances for <span class="label label-warning">average</span></th>
			<th>Chances for <span class="label label-info">great</span></b></th>
		</tr>
		<tr>
			<td class="{{ scoredResultColor scored_result }}"><b>{{ scoredResult scored_result }}</b></td>
			<td>{{ makePercentage probabilities_1 }}%</td>
			<td>{{ makePercentage probabilities_2 }}%</td>
			<td>{{ makePercentage probabilities_3 }}%</td>
		</tr>
	</table>
</script>