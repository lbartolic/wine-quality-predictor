<script id="newWineResultsInputs-hbt" type="text/x-handlebars-template">
	<input type="hidden" value="{{ fixed_acidity }}" name="fixed_acidity">
	<input type="hidden" value="{{ citric_acid }}" name="citric_acid">
	<input type="hidden" value="{{ sugar }}" name="sugar">
	<input type="hidden" value="{{ chlorides }}" name="chlorides">
	<input type="hidden" value="{{ density }}" name="density">
	<input type="hidden" value="{{ ph }}" name="ph">
	<input type="hidden" value="{{ sulphates }}" name="sulphates">
	<input type="hidden" value="{{ alcohol }}" name="alcohol">
	<input type="hidden" value="{{ probabilities_1 }}" name="probabilities_1">
	<input type="hidden" value="{{ probabilities_2 }}" name="probabilities_2">
	<input type="hidden" value="{{ probabilities_3 }}" name="probabilities_3">
	<input type="hidden" value="{{ scored_result }}" name="scored_result">
	<input type="hidden" value="{{ wine_type }}" name="wine_type">
</script>