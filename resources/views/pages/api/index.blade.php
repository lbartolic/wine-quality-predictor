@extends('app')

@section('pageTitle', 'My API')

@section('navbar')

@include('partials.navbar')

@endsection

@section('main')

<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
			<div class="well clearfix _content-card">
				<h3 class="_content__title">My API Details</h3>
				<span class="label label-default _label-med">API key</span>
				<p class="_text-break _marginTop6"><b>{{ Auth::user()->api_key }}</b></p>
				<span class="label label-default _label-med">API call example</span>
				<p class="_marginTop6 _text-break">
					<samp>{{ url('/api') }}/<code>{{ Auth::user()->api_key }}</code>/wines?scored_result=<code>2</code>&sort=<code>asc</code>&limit=<code>10</code></samp>
				</p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-offset-1 col-lg-10">
			<div class="well clearfix _content-card">
				<h3 class="_content__title">My API Docs</h3>
				<h4 class="_api-doc-title" data-toggle="collapse" href="#collapseAllWines" aria-expanded="true">All Wines</h4>
				<div id="collapseAllWines" class="panel-collapse collapse">
					<span class="label label-default _label-med">API call</span>
					<p class="_marginTop6 _text-break"><samp>{{ url('/api') }}/<code>{YOUR_API_KEY}</code>/wines</samp></p>
					<span class="label label-default _label-med">Parameters</span>
					<p class="_marginTop6"><code>{YOUR_API_KEY}</code> API key associated with your account</p>
				</div>

				<h4 class="_api-doc-title _marginTop15" data-toggle="collapse" href="#collapseWinesWithParams" aria-expanded="true">All Wines with Parameters</h4>
				<div id="collapseWinesWithParams" class="panel-collapse collapse in">
					<span class="label label-default _label-med">API call</span>
					<p class="_marginTop6 _text-break"><samp>{{ url('/api') }}/<code>{YOUR_API_KEY}</code>/wines?scored_result=<code>{scored_result}</code>&sort=<code>{sort}</code>&limit=<code>{limit}</code></samp></p>
					<span class="label label-default _label-med">Parameters</span>
					<p class="_marginTop6"><code>{YOUR_API_KEY}</code> API key associated with your account</p>
					<p class="_marginTop6"><code>{scored_result}</code> <b>1</b> = bad, <b>2</b> = average, <b>3</b> = great</p>
					<p class="_marginTop6"><code>{sort}</code> <b>ASC</b> or <b>DESC</b></p>
					<p class="_marginTop6"><code>{limit}</code> number of items to return</p>
					<span class="label label-default _label-med">Example of API call</span>
					<p class="_marginTop6 _text-break"><samp>{{ url('/api') }}/<code>{YOUR_API_KEY}</code>/wines?scored_result=<code>2</code>&sort=<code>asc</code>&limit=<code>10</code></samp></p>
				</div>

				<h4 class="_api-doc-title _marginTop15" data-toggle="collapse" href="#collapseWhiteWines" aria-expanded="true">White Wines</h4>
				<div id="collapseWhiteWines" class="panel-collapse collapse">
					<span class="label label-default _label-med">API call</span>
					<p class="_marginTop6 _text-break"><samp>{{ url('/api') }}/<code>{YOUR_API_KEY}</code>/wines/white</samp></p>
					<span class="label label-default _label-med">Parameters</span>
					<p class="_marginTop6">same as for "All Wines"</p>
				</div>

				<h4 class="_api-doc-title _marginTop15" data-toggle="collapse" href="#collapseWhiteWithParams" aria-expanded="true">White Wines with Parameters</h4>
				<div id="collapseWhiteWithParams" class="panel-collapse collapse">
					<span class="label label-default _label-med">API call</span>
					<p class="_marginTop6 _text-break"><samp>{{ url('/api') }}/<code>{YOUR_API_KEY}</code>/wines/white?scored_result=<code>{scored_result}</code>&sort=<code>{sort}</code>&limit=<code>{limit}</code></samp></p>
					<span class="label label-default _label-med">Parameters</span>
					<p class="_marginTop6">same as for "All Wines"</p>
				</div>

				<h4 class="_api-doc-title _marginTop15" data-toggle="collapse" href="#collapseRedWines" aria-expanded="true">Red Wines</h4>
				<div id="collapseRedWines" class="panel-collapse collapse">
					<span class="label label-default _label-med">API call</span>
					<p class="_marginTop6 _text-break"><samp>{{ url('/api') }}/<code>{YOUR_API_KEY}</code>/wines/red</samp></p>
					<span class="label label-default _label-med">Parameters</span>
					<p class="_marginTop6">same as for "All Wines"</p>
				</div>

				<h4 class="_api-doc-title _marginTop15" data-toggle="collapse" href="#collapseRedWithParams" aria-expanded="true">Red Wines with Parameters</h4>
				<div id="collapseRedWithParams" class="panel-collapse collapse">	
					<span class="label label-default _label-med">API call</span>
					<p class="_marginTop6 _text-break"><samp>{{ url('/api') }}/<code>{YOUR_API_KEY}</code>/wines/red?scored_result=<code>{scored_result}</code>&sort=<code>{sort}</code>&limit=<code>{limit}</code></samp></p>
					<span class="label label-default _label-med">Parameters</span>
					<p class="_marginTop6">same as for "All Wines"</p>
				</div>

			</div>
		</div>
	</div>
</div>
<div class="_fade"></div>

@endsection