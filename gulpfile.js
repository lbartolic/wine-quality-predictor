var elixir = require('laravel-elixir');

elixir(function(mix) {
	mix.sass([
		"./resources/assets/sass/login.scss",
		"./resources/assets/sass/app.scss"
	], "./resources/assets/sass/app_compiled");
	mix.styles([
	    "./resources/assets/css/animate.css",
		"./resources/assets/css/bootstrap.min.css",
		"./resources/assets/css/bootstrap-datetimepicker.min.css",
		"./resources/assets/css/formvalidatorTheme.min.css",
		"./resources/assets/css/select2.min.css",
		"./resources/assets/css/font-awesome.min.css",
		"./resources/assets/css/sweetalert.css",
		"./resources/assets/css/nanoscroller.css",
		"./resources/assets/css/lobibox.min.css",
		"./resources/assets/sass/app_compiled"
	], 'public/assets/css/all.css');
	mix.scripts([
	    "./resources/assets/js/jquery-1.10.2.js",
		"./resources/assets/js/jquery-ui.js",
		"./resources/assets/js/bootstrap.min.js",
		"./resources/assets/js/moment.js",
		"./resources/assets/js/bootstrap-datetimepicker.min.js",
		"./resources/assets/js/select2.full.min.js",
		"./resources/assets/js/chartjs.min.js",
		"./resources/assets/js/jquery.form-validator.min.js",
		"./resources/assets/js/underscore.min.js",
		"./resources/assets/js/handlebars-v4.0.5.js",
		"./resources/assets/js/handlebars-intl.min.js",
		"./resources/assets/js/hr.js",
		"./resources/assets/js/sweetalert.min.js",
		"./resources/assets/js/nanoscroller.min.js",
		"./resources/assets/js/lobibox.min.js",
		"./resources/assets/js/app/handlebars-helpers.js",
		"./resources/assets/js/app/charts.js",
		"./resources/assets/js/app/source.js"
	], 'public/assets/js/all.js');
});
