<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScoresFieldsToWinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wines', function (Blueprint $table) {
            $table->float('probabilities_1');
            $table->float('probabilities_2');
            $table->float('probabilities_3');
            $table->float('probabilities_4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wines', function (Blueprint $table) {
            $table->dropColumn('probabilities_1');
            $table->dropColumn('probabilities_2');
            $table->dropColumn('probabilities_3');
            $table->dropColumn('probabilities_4');
        });
    }
}
